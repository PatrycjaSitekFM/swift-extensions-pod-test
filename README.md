# SwiftExtensionsPodTest

[![CI Status](https://img.shields.io/travis/psitekfm/SwiftExtensionsPodTest.svg?style=flat)](https://travis-ci.org/psitekfm/SwiftExtensionsPodTest)
[![Version](https://img.shields.io/cocoapods/v/SwiftExtensionsPodTest.svg?style=flat)](https://cocoapods.org/pods/SwiftExtensionsPodTest)
[![License](https://img.shields.io/cocoapods/l/SwiftExtensionsPodTest.svg?style=flat)](https://cocoapods.org/pods/SwiftExtensionsPodTest)
[![Platform](https://img.shields.io/cocoapods/p/SwiftExtensionsPodTest.svg?style=flat)](https://cocoapods.org/pods/SwiftExtensionsPodTest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SwiftExtensionsPodTest is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SwiftExtensionsPodTest'
```

## Author

psitekfm, p.sitek@futuremind.com

## License

SwiftExtensionsPodTest is available under the MIT license. See the LICENSE file for more info.
